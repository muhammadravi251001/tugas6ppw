from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import *
from .forms import KegiatanForm
from django.shortcuts import render


def webpage1(request):
    return render(request, 'home/pageSelamatDatang.html')

def webpage2(request):
    return render(request, 'home/tambah.html')

#def webpage3(request):
#    return render(request, 'home/hapus.html')

def webpage4(request):
    kegiatans = Kegiatan.objects.all()
    context = {'kegiatans':kegiatans}
    return render(request, 'home/lihat.html', context)

def webpage5(request):
    kegiatans = Kegiatan.objects.all()
    context = {'kegiatans':kegiatans}
    return render(request, 'home/lihatNonDetail.html', context)

def tambahSubmisi(request):
    form = KegiatanForm()
    if (form.is_valid and request.method == 'POST'):
        form = KegiatanForm(request.POST)
        form.save()
        return redirect('/lihatNonDetail')
    else:
        return redirect('/tambah')
    context = {'form':form}
    return render(request, 'home/lihatNonDetail.html', context)

def updateKegiatan(request, pk):
    kegiatan = Kegiatan.objects.get(id=pk)
    form = KegiatanForm(instance=kegiatan)

    if (form.is_valid and request.method == 'POST'):
        form = KegiatanForm(request.POST, instance=kegiatan)
        form.save()
        return redirect('/lihatNonDetail')

    context = {'form':form}
    return render(request, 'home/kegiatan_update.html', context)

def deleteKegiatan(request, pk):
    kegiatan = Kegiatan.objects.get(id=pk)
    
    if request.method == "POST":
        kegiatan.delete()
        return redirect('/lihatNonDetail')

    context = {'kegiatan':kegiatan}
    return render(request, 'home/kegiatan_delete.html', context)


def detailKegiatan(request, pk):
    kegiatan = Kegiatan.objects.all().filter(id=pk)
    context = {'kegiatan':kegiatan}
    return render(request, 'home/lihatDetail.html', context)


