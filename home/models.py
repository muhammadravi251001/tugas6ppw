from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    nama_peserta = models.CharField(max_length=30)
    deskripsi_kegiatan = models.CharField(max_length=50)
    tanggal_kegiatan = models.CharField(max_length=10)
    jam_kegiatan = models.CharField(max_length=10)

    def __str__(self):
        return self.nama_kegiatan

#    phone_number = models.CharField(max_length=17)
#    id_line = models.CharField(max_length=6)

#class Post(models.Model):
#    author = models.ForeignKey(Person, on_delete =   
#      models.CASCADE)
#   content = models.CharField(max_length=125)
#    published_date =   
#      models.DateTimeField(default=timezone.now)


