# Generated by Django 3.1.1 on 2020-10-21 03:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.CharField(max_length=30)),
                ('nama_peserta', models.CharField(max_length=30)),
                ('tanggal_kegiatan', models.CharField(max_length=10)),
            ],
        ),
        migrations.DeleteModel(
            name='MataKuliah',
        ),
    ]
